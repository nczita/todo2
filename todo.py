from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
import logging


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.sql'
app.config['FLASK_DEBUG'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
db = SQLAlchemy(app)

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


log = logging.getLogger(__name__)

class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(1000))
    title = db.Column(db.String(120))

    def __init__(self, url, title):
        self.url = url
        self.title = title

@app.route("/")
def hello():
    log.info("Hello there!")
    links = Link.query.all()
    return render_template("index.html", links=links)


@app.route("/link", methods=["POST"])
def add_link():
    url = request.form['url']
    title = request.form['title']
    if title.strip() != "":
        l = Link(url, title)
        db.session.add(l)
        db.session.commit()
        log.info("Added new link!")
    else:
        log.info("Link need title!")
    return redirect(url_for('.hello'))

@app.route("/link/<_id>", methods=["GET"])
def show_link(_id):
    log.info("Show link")
    link = Link.query.get_or_404(_id)
    return render_template('link.html', link=link)


if __name__ == "__main__":
    if app.config['SQLALCHEMY_DATABASE_URI'] == 'sqlite://':
        db.create_all()
        l1 = Link("https://www.airhelp.com", "AirHelp")
        l2 = Link("https://bitbucket.org/nczita", "Nczita BitBucket")
        db.session.add(l1)
        db.session.add(l2)
        db.session.commit()
    app.run()

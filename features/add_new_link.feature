# add_new_link.feature
Feature: Adding new todo note via REST
  In order to add new todo note
  As a application user
  I want application to allow creating it by REST request
  so frontend application could do the same

  Scenario: Adding new valid todo
    Given todo application have some todo notes
    When add valid todo note via REST
    Then todo application have one more todo note

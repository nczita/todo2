# features/environment.py
from behave import fixture, use_fixture

@fixture
def base_url(context):
    context.base_url = "http://127.0.0.1:5000"

def before_all(context):
    use_fixture(base_url, context)


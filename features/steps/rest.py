# features/steps/rest.py
import requests
from bs4 import BeautifulSoup

@given(u'todo application have some todo notes')
def step_impl(context):
    response = requests.get(context.base_url)
    soup = BeautifulSoup(response.content, 'html.parser')
    context.number_of_todo_notes = len(soup.select('ul > li'))

@when(u'add valid todo note via REST')
def step_impl(context):
    response = requests.post(context.base_url + '/link', data={'url' : 'https://newlink.example.net', 'title': 'New link'})
    assert response.ok

@then(u'todo application have one more todo note')
def step_impl(context):
    response = requests.get(context.base_url)
    soup = BeautifulSoup(response.content, 'html.parser')
    assert context.number_of_todo_notes < len(soup.select('ul > li'))